// Get file path of this script's project
Function ScriptProjectPath() : String;
Var 
  Workspace : IWorkspace;
  Project   : IProject;
  scriptsPath : TDynamicString;
  projectCount : Integer;
  i      : Integer;
Begin
  // Attempt to get reference to current workspace. }
  Workspace  := GetWorkspace;
  If (Workspace = Nil) Then
    Begin
      return := '';
      exit;
    End;

  // Get a count of the number of currently opened projects. 
  // The script project from which this script runs must be one of these.
  projectCount := Workspace.DM_ProjectCount();

  // Loop over all the open projects.
  // NOTE: Expecting only one project to be open.
  scriptsPath := '';
  For i:=0 To projectCount-1 Do
    Begin
      // Get reference to project # i.
      Project := Workspace.DM_Projects(i);
      // See if we found our script project.
      If (Project.DM_ProjectFullPath <> 'Free Documents') Then
        Begin
          // Strip off project name to give us just the path.
          scriptsPath := Project.DM_ProjectFullPath;
        End;
    End;
  result := scriptsPath;
End;

// Append to an existing file
Function AppendToFile(f : TextFile; msg : String);
Begin
  Append(f);
  WriteLn(f, msg);
  CloseFile(f);
End;

// Generate output files for build and release system
Procedure GenerateBuildOutputs(variant : String);
Var 
  ProjectFilePath             : String;
  WS                          : IWorkspace;
  Prj                         : IProject;
  Build_OutJob                : IWSM_OutputJobDocument;
  logfile                     : TextFile;
Begin
  // Set the Project file path
  ProjectFilePath := ScriptProjectPath();

  // Initialize log file
  AssignFile(logfile, ExtractFilePath(ProjectFilePath) + 'altium_build.log');
  ReWrite(logfile);
  CloseFile(logfile);
  AppendToFile(logfile, DateTimeToStr(Now) + ': ' +
  'Altium Designer build process started.');
  AppendToFile(logfile, DateTimeToStr(Now) + ': ' +
  'Project file path discovered as ' + ProjectFilePath);

  // Reset all parameters
  ResetParameters;

  // Open the project
  AddStringParameter('ObjectKind','Project');
  AddStringParameter('FileName', ProjectFilePath);
  RunProcess('WorkspaceManager:OpenObject');
  ResetParameters;
  AppendToFile(logfile, DateTimeToStr(Now) + ': ' + 'Project has been opened.');

  Build_OutJob := Client.OpenDocument('OUTPUTJOB', ExtractFilePath(
                  ProjectFilePath) + 'Build.OutJob');
  Build_OutJob.VariantName := variant;
  WS := GetWorkspace;
  If WS <> Nil Then
    Begin
      Prj := WS.DM_FocusedProject;
      If Prj <> Nil Then
        Begin
          // Compile the project
          Prj.DM_Compile;
          AppendToFile(logfile, DateTimeToStr(Now) + ': ' +
          'Project has been compiled.');

          // Generate files to Folder type Output Container (Build)
          If Build_OutJob <> Nil Then
            Begin
              // Open up and show the Output Job file
              AppendToFile(logfile, DateTimeToStr(Now) + ': ' +
              'Opening Build.OutJob.');
              Client.ShowDocument(Build_OutJob);

              // Set up Output Job Parameters for Build files and generate
              AddStringParameter('ObjectKind', 'OutputBatch');
              AddStringParameter('DisableDialog', 'True');
              AddStringParameter('OutputMedium', 'Package');
              AddStringParameter('Action', 'Run');
              RunProcess('WorkSpaceManager:GenerateReport');
              ResetParameters;
              AppendToFile(logfile, DateTimeToStr(Now) + ': ' +
              'Build files have been generated.');

              // Set up Output Job Parameters for PDF and generate
              AddStringParameter('ObjectKind', 'OutputBatch');
              AddStringParameter('DisableDialog', 'True');
              AddStringParameter('OutputMedium', 'Documentation');
              AddStringParameter('Action', 'PublishToPDF');
              RunProcess('WorkSpaceManager:Print');
              ResetParameters;
              AppendToFile(logfile, DateTimeToStr(Now) + ': ' +
              'Schematic PDF has been generated.');

              AppendToFile(logfile, DateTimeToStr(Now) + ': ' +
              'Build.OutJob operation complete.');
            End

            // Build_OutJob is empty. Exit with error code.
          Else
            Begin
              AppendToFile(logfile, DateTimeToStr(Now) + ': ' +
              'ERROR: Build_OutJob is empty. Exiting with error code.');
              TerminateWithExitCode(1);
            End;
        End
        // Prj is empty. Exit with error code.
      Else
        Begin
          AppendToFile(logfile, DateTimeToStr(Now) + ': ' +
          'ERROR: Prj is empty. Exiting with error code.');
          TerminateWithExitCode(1);
        End;
    End
    // WS is empty. Exit with error code.
  Else
    Begin
      AppendToFile(logfile, DateTimeToStr(Now) + ': ' +
      'ERROR: WS is empty. Exiting with error code.');
      TerminateWithExitCode(1);
    End;

  // Close and save all objects
  AddStringParameter('ObjectKind','All');
  AddStringParameter('ModifiedOnly','True');
  RunProcess('WorkspaceManager:SaveObject');
  RunProcess('WorkspaceManager:CloseObject');
  ResetParameters;
  AppendToFile(logfile, DateTimeToStr(Now) + ': ' + 'All objects closed.');

  // Close log file
  AppendToFile(logfile, DateTimeToStr(Now) + ': ' +
  'Altium Designer build complete. Terminating Altium Designer.');

  // Close Altium Designer
  TerminateWithExitCode(0);
End;

Procedure GenerateOutputFiles;
Begin
  GenerateBuildOutputs('Default_assembly');
End;
